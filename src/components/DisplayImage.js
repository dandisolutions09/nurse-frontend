import React, { useState, useEffect } from 'react';
import ImageRenderer from './ImageRenderer';

const ImageGallery = () => {
  const [images, setImages] = useState([]);
  const [imageIds, setImageIds] = useState([]);


  useEffect(() => {
    const fetchImages = async () => {
      try {
        const response = await fetch('https://cc8e-41-80-117-206.ngrok-free.app/get-images/');
        const data = await response.json();
        console.log("data", data)
        setImages(data);
        setImageIds(data._id)
      } catch (error) {
        console.error('Error fetching images:', error);
      }
    };

    fetchImages();
  }, []);

  return (
    <div>
      <h1>Image Gallery</h1>
      {images.map((image) => (
        <div key={image._id}>
          {/* <img src={`data:image/jpeg;base64,${image.data}`} alt={image.name} /> */}
          <p>Nurse name:- {image.name}</p>
          <img src={image.signatureImageData} alt={image.name} />

          {/* <ImageRenderer imageId={image._id} /> */}

        </div>
      ))}
    </div>
  );
};

export default ImageGallery;
