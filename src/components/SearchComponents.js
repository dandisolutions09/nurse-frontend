import React, { useRef, useState } from "react";
import useGetNurseByName from "../hooks/SearchNurseHook";
import "./Search.css"; // Import your CSS file
import { useNavigate } from "react-router-dom";
import health_logo from "../assets/Logo_lyteware_big-removebg4.png";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Stack,
  Heading,
  Text,
  StackDivider,
  Box,
  Alert,
  AlertIcon,
  Image,
  HStack,
  Input,
  VStack,
  Button,
  useDisclosure,
  FormControl,
  FormLabel,
  FormErrorMessage,
  useToast,
} from "@chakra-ui/react";

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from "@chakra-ui/react";

import { ChevronDownIcon } from "@chakra-ui/icons";

import {
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  MenuItemOption,
  MenuGroup,
  MenuOptionGroup,
  MenuDivider,
} from "@chakra-ui/react";
import { Field, Form, Formik } from "formik";
import axios from "axios";

function NurseSearch() {
  const [searchName, setSearchName] = useState("");
  const [loginSuccess, setLoginSuccess] = useState("");
  const toast = useToast();

  const { nurseData, loading, error, getNurseByName } = useGetNurseByName();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [openModal, setOpenModal] = useState(false);

  const openSesame = () => {
    console.log("open sesame function called!");
    setOpenModal(true);
  };

  const closeSesame = () => {
    console.log("close sesame function called!");
    setOpenModal(false);
  };

  const LoginRef = useRef({
    username: "",
  });

  const navigate = useNavigate();
  // console.log("nursedata", nurseData);

  const handleSearch = () => {
    if (!searchName) {
      alert("Please enter a nurse's name.");
      return;
    }
    getNurseByName(searchName);
  };

  const handleAddNurse = () => {
    // getNurseByName(searchName);
    console.log("Add nurse clicked");

    navigate("/new-nurse");
  };

  const handleAdminLogin = () => {
    console.log("Adming Login function called");
  };

  const handleEditNurse = () => {
    //setOpenModal(true)
    openSesame();
    // getNurseByName(searchName);
    console.log("Edit Nurse clicked", nurseData);
    //navigate("/receipt", { state: cardData });

    // navigate("/edit-nurse", { state: nurseData });
    //navigate("/edit-nurse2", { state: nurseData });
  };

  const handleDelete = async (nurse_data) => {
    console.log("nurse data", nurse_data);

    try {
      const response = await axios.delete(
        `https://rpmtms.online/delete-nurse/${nurse_data._id}`,
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      // Handle success
      console.log("");
      toast({
        title: "Nurse Deleted successfully",
        position: "top",
        isClosable: true,
        duration: 3000,
        status: "success",
      });

      window.location.reload();
      console.log("should be navigatiing...");
    } catch (error) {
      // Handle error
      toast({
        title: "Cannot be deleted",
        position: "top",
        isClosable: true,
        duration: 3000,
        status: "error",
      });
    }
  };

  return (
    <>
      <Box position="absolute" top="5%" left="80%">
        <Menu>
          <MenuButton
            as={Button}
            colorScheme="teal"
            rightIcon={<ChevronDownIcon />}
          >
            Actions
          </MenuButton>
          <MenuList>
            <MenuItem onClick={onOpen}>Add New Nurse</MenuItem>
          </MenuList>
        </Menu>

        {/* Add  nurse information modal */}
        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Admin Login</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <Formik
                initialValues={{
                  // firstName: "",
                  // lastName: "",
                  username: "",
                  password: "",
                }}
                onSubmit={(values, actions) => {
                  // setLoading(true);

                  console.log("values", values);

                  const formData = new FormData();
                  formData.append("username", values.username);
                  formData.append("password", values.password);

                  LoginRef.current.username = values.username;

                  if (
                    values.username === "admin" &&
                    values.password === "1234"
                  ) {
                    console.log("Login successful");
                    setLoginSuccess(true);
                    toast({
                      title: "Login successfully",
                      position: "top",
                      isClosable: true,
                      duration: 3000,
                      status: "success",
                    });
                    navigate("/new-nurse");
                  } else {
                    console.log("Login failed");
                    toast({
                      title: "incorrect Password",
                      position: "top",
                      isClosable: true,
                      duration: 3000,
                      status: "error",
                    });
                  }

                  // post("http://localhost:8000/token", formData);

                  // navigate("/registration", { state: cardData });
                }}

                //   console.log("reponse:", response);
                //   setError(response.error)
              >
                {(props) => (
                  <Form>
                    {error && <Box mb={14}></Box>}

                    <Box mb={6}>
                      <Field name="username">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.username && form.touched.username
                            }
                          >
                            <FormLabel>Username</FormLabel>
                            <Input {...field} placeholder="Username" />
                            <FormErrorMessage>
                              {form.errors.username}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>
                    </Box>

                    <Box mb={6}>
                      <Field name="password">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.password && form.touched.password
                            }
                          >
                            <FormLabel> Password</FormLabel>
                            <Input
                              type="password"
                              {...field}
                              placeholder="Password"
                            />

                            <FormErrorMessage>
                              {form.errors.password}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>
                    </Box>

                    {/* <Button
                      mt={6}
                      colorScheme="teal"
                      // isLoading={props.isSubmitting}
                      isLoading={loading}
                      type="submit"
                      // isDisabled={isError} // Disable the button if any required field is empty
                    >
                      Login
                    </Button> */}
                    <ModalFooter>
                      <Button colorScheme="red" mr={3} onClick={closeSesame}>
                        Close
                      </Button>
                      {/* <Button type="submit" colorScheme="blue" >Login</Button> */}

                      <Button
                        // mt={6}
                        colorScheme="teal"
                        // isLoading={props.isSubmitting}
                        isLoading={loading}
                        type="submit"
                        // isDisabled={isError} // Disable the button if any required field is empty
                      >
                        Login
                      </Button>
                    </ModalFooter>
                  </Form>
                )}
              </Formik>
            </ModalBody>
          </ModalContent>
        </Modal>

        {/* Edit nurse information modal */}
        <Modal isOpen={openModal} onClose={closeSesame}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Admin Login</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <Formik
                initialValues={{
                  // firstName: "",
                  // lastName: "",
                  username: "",
                  password: "",
                }}
                onSubmit={(values, actions) => {
                  // setLoading(true);

                  console.log("values", values);

                  const formData = new FormData();
                  formData.append("username", values.username);
                  formData.append("password", values.password);

                  LoginRef.current.username = values.username;

                  if (
                    values.username === "admin" &&
                    values.password === "1234"
                  ) {
                    console.log("Login successful");
                    setLoginSuccess(true);
                    toast({
                      title: "Login successfully",
                      position: "top",
                      isClosable: true,
                      duration: 3000,
                      status: "success",
                    });
                    // navigate("/edit-nurse2");
                    navigate("/edit-nurse2", { state: nurseData });
                  } else {
                    console.log("Login failed");
                    toast({
                      title: "incorrect Password",
                      position: "top",
                      isClosable: true,
                      duration: 3000,
                      status: "error",
                    });
                  }

                  // post("http://localhost:8000/token", formData);

                  // navigate("/registration", { state: cardData });
                }}

                //   console.log("reponse:", response);
                //   setError(response.error)
              >
                {(props) => (
                  <Form>
                    {error && <Box mb={14}></Box>}

                    <Box mb={6}>
                      <Field name="username">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.username && form.touched.username
                            }
                          >
                            <FormLabel>Username</FormLabel>
                            <Input {...field} placeholder="Username" />
                            <FormErrorMessage>
                              {form.errors.username}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>
                    </Box>

                    <Box mb={6}>
                      <Field name="password">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.password && form.touched.password
                            }
                          >
                            <FormLabel> Password</FormLabel>
                            <Input
                              type="password"
                              {...field}
                              placeholder="Password"
                            />

                            <FormErrorMessage>
                              {form.errors.password}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>
                    </Box>

                    {/* <Button
                      mt={6}
                      colorScheme="teal"
                      // isLoading={props.isSubmitting}
                      isLoading={loading}
                      type="submit"
                      // isDisabled={isError} // Disable the button if any required field is empty
                    >
                      Login
                    </Button> */}
                    <ModalFooter>
                      <Button colorScheme="red" mr={3} onClick={closeSesame}>
                        Close
                      </Button>
                      {/* <Button type="submit" colorScheme="blue" >Login</Button> */}

                      <Button
                        // mt={6}
                        colorScheme="teal"
                        // isLoading={props.isSubmitting}
                        isLoading={loading}
                        type="submit"
                        // isDisabled={isError} // Disable the button if any required field is empty
                      >
                        Login
                      </Button>
                    </ModalFooter>
                  </Form>
                )}
              </Formik>
            </ModalBody>
          </ModalContent>
        </Modal>
      </Box>

      <div className="container">
        <Box>
          <Image width={400}  src={health_logo} alt="Dan Abramov" />
        </Box>
        <HStack>
          <Input
            placeholder="Search"
            onChange={(e) => setSearchName(e.target.value)}
          />

          <Button colorScheme="teal" onClick={handleSearch}>
            Search
          </Button>
        </HStack>

        {loading && <p>Loading...</p>}
        {error && (
          <Box>
            {nurseData && nurseData.message == "Document not found" && (
              <Alert status="error">
                <AlertIcon />
                Nurse name not found
              </Alert>
            )}
          </Box>
        )}

        {/* <Box className="query-container" mt={3} width="60%"> */}
        {nurseData && nurseData.message !== "Document not found" && (
          <Box className="query-container" mt={3} width="60%">
            <Card>
              {/* <Text>{nurseData.message}</Text> */}

              <HStack direction={"row"}>
                <CardHeader>
                  <Heading size="md">Query Results:</Heading>
                </CardHeader>

                {!error && nurseData && (
                  <>
                     <HStack justifyContent={"space-between"} alignItems={"center"} w={"full"}>
                    <Button
                      colorScheme="green"
                      onClick={() => handleEditNurse(nurseData)}
                      // onClick={openSesame}
                      // className="edit-nurse-button"
                    >
                      Edit Nurse
                    </Button>
                 
                      <Button
                        colorScheme="red"
                        onClick={() => handleDelete(nurseData)}
                        // onClick={openSesame}
                        // className="edit-nurse-button"
                      >
                        Delete Nurse
                      </Button>
                    </HStack>

                    {/* <Button colorScheme='blue'>Button</Button> */}
                  </>
                )}
              </HStack>

              <CardBody>
                <Stack divider={<StackDivider />} spacing="4">
                  <Box>
                    <Heading size="xs" textTransform="uppercase">
                      Name:
                    </Heading>
                    <Text pt="1" fontSize="sm">
                      {nurseData.name}
                    </Text>
                  </Box>
                  <Box>
                    <Heading size="xs" textTransform="uppercase">
                      Department:
                    </Heading>
                    <Text pt="1" fontSize="sm">
                      {nurseData.department}
                    </Text>
                  </Box>
                  <Box>
                    <Heading size="xs" textTransform="uppercase">
                      Wards:
                    </Heading>
                    <Text pt="2" fontSize="sm">
                      {nurseData.wards}
                    </Text>
                  </Box>

                  <Box>
                    <Heading size="xs" textTransform="uppercase">
                      Grade:
                    </Heading>
                    <Text pt="2" fontSize="sm">
                      {nurseData.grade}
                    </Text>
                  </Box>

                  <Box>
                    <Heading size="xs" textTransform="uppercase">
                      Signature:
                    </Heading>
                    {/* <Text pt="2" fontSize="sm">
                    {nurseData.signature}
                  </Text> */}

                    <Image
                      src={nurseData.signatureImageData}
                      alt="Dan Abramov"
                    />
                  </Box>
                </Stack>
              </CardBody>
            </Card>
          </Box>
        )}
      </div>
    </>
  );
}

export default NurseSearch;
