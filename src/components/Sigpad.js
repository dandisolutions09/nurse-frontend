import React, { useState } from "react";
import SignatureCanvas from "react-signature-canvas";
import axios from "axios";
import "./Search.css"; // Import your CSS file
import { useNavigate } from "react-router-dom";
import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
  Input,
  Button,
} from "@chakra-ui/react";
import { Field, Formik,Form  } from "formik";
const SignatureCapture = () => {
  // Ref to the signature canvas
  const sigCanvasRef = React.useRef(null);
  const navigate = useNavigate();

  // State for name, department, wards, grade, and signature image data
  const [name, setName] = useState("");
  const [department, setDepartment] = useState("");
  const [wards, setWards] = useState([]);
  const [grade, setGrade] = useState("");
  const [imageData, setImageData] = useState("");

  // Function to clear the signature canvas
  const clearSignature = () => {
    sigCanvasRef.current.clear();
  };

  // Function to save the signature, name, department, wards, and grade to the backend
  const saveSignature = async () => {
    if (sigCanvasRef.current.isEmpty()) {
      alert("Please provide your signature.");
      return;
    }

    // Get the signature image data as a Base64-encoded URL
    const signatureImageURL = sigCanvasRef.current.toDataURL();

    // Create the data object
    const data = {
      name: name,
      department: department,
      wards: wards,
      grade: grade,
      signatureImageData: signatureImageURL,
    };

    // Send the data to the backend
    try {
      const response = await axios.post(
        "https://rpmtms.online/your-fastapi-endpoint2",
        data
      );

      // Handle the response from the backend if needed
      console.log("Response from the backend:", response.data);
    } catch (error) {
      // Handle any errors that occur during the request
      console.error("Error sending data to the backend:", error);
    }
    alert("nurse saved successfully...");

    navigate("/");
  };

  return (
    <div className="container">
      <h2>Signature Capture</h2>

      <div>
        <div>
          {/* <label htmlFor="name">Name:</label> */}
          <input
            type="text"
            id="name"
            placeholder="name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            className="search-input"
          />
        </div>
        <div>
          {/* <label htmlFor="department">Department:</label> */}
          <input
            type="text"
            id="department"
            placeholder="department"
            value={department}
            onChange={(e) => setDepartment(e.target.value)}
            className="search-input"
          />
        </div>
        <div>
          {/* <label htmlFor="wards">Wards (comma-separated):</label> */}
          <input
            type="text"
            id="wards"
            placeholder="wards"
            value={wards.join(",")}
            onChange={(e) => setWards(e.target.value.split(","))}
            className="search-input"
          />
        </div>
        <div>
          {/* <label htmlFor="grade">Grade:</label> */}
          <input
            type="text"
            id="grade"
            placeholder="grade"
            value={grade}
            onChange={(e) => setGrade(e.target.value)}
            className="search-input"
          />
        </div>
        <SignatureCanvas
          ref={sigCanvasRef}
          penColor="black"
          canvasProps={{
            width: 500,
            height: 200,
            className: "signature-canvas",
            className: "image-container",
          }}
        />
      </div>
      <div>
        <button onClick={clearSignature}>Clear</button>
        <button onClick={saveSignature}>Save Signature</button>
      </div>

      <>
        <Formik
          initialValues={{ name: "Sasuke" }}
          onSubmit={(values, actions) => {
            setTimeout(() => {
              alert(JSON.stringify(values, null, 2));
              actions.setSubmitting(false);
            }, 1000);
          }}
        >
          {(props) => (
            <Form>
              <Field name="name" >
                {({ field, form }) => (
                  <FormControl
                    isInvalid={form.errors.name && form.touched.name}
                  >
                    <FormLabel>First name</FormLabel>
                    <Input {...field} placeholder="name" />
                    <FormErrorMessage>{form.errors.name}</FormErrorMessage>
                  </FormControl>
                )}
              </Field>
              <Button
                mt={4}
                colorScheme="teal"
                isLoading={props.isSubmitting}
                type="submit"
              >
                Submit
              </Button>
            </Form>
          )}
        </Formik>
      </>
    </div>
  );
};

export default SignatureCapture;
