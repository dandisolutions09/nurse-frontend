import React, { useState, useEffect } from 'react';

const ImageRenderer = ({ imageId }) => {
  const [imageData, setImageData] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    // Make a GET request to the API endpoint to retrieve the image data
    fetch(`https://rpmtms.online/image/${imageId}/`)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.blob();
      })
      .then((blobData) => {
        // Convert the blob data to a data URL
        const reader = new FileReader();
        reader.onloadend = () => {
          setImageData(reader.result);
          setLoading(false);
        };
        reader.readAsDataURL(blobData);
      })
      .catch((error) => {
        console.error('Error fetching image:', error);
        setLoading(false);
      });
  }, [imageId]);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (!imageData) {
    return <div>Error: Image not found</div>;
  }

  return <img src={imageData} alt="API Image" />;
};

export default ImageRenderer;
