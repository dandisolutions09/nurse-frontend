import React, { useState, useEffect } from "react";
import "./Search.css"; // Import your CSS file
import SignatureCanvas from "react-signature-canvas";
import { useLocation } from "react-router-dom";
import { useNavigate } from "react-router-dom";

function EditNurseForm() {
  const location = useLocation(); // Use the useLocation hook to get the location state
  const nurseData = location.state;
  const navigate = useNavigate();

  console.log("edited data:", nurseData);

  const sigCanvasRef = React.useRef(null);

  const [nurseId, setNurseId] = useState(nurseData._id); // Nurse ObjectId
  const [name, setName] = useState(nurseData.name);
  const [department, setDepartment] = useState(nurseData.department);
  const [wards, setWards] = useState([nurseData.wards]);
  const [grade, setGrade] = useState(nurseData.grade);
  const [signatureImageData, setSignatureImageData] = useState(
    nurseData.signatureImageData
  );

  const handlePUTrequest = async (nurseId) => {
    try {
      const response = await fetch(
        `https://rpmtms.online/edit-nurse/${nurseId}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: name,
            department: department,
            wards: wards,
            grade: grade,
            signatureImageData: signatureImageData,
          }),
        }
      );

      if (response.ok) {
        console.log("Nurse information updated successfully");
        // Handle further actions or UI updates
        //console.log("body", signatureImageData);
      } else {
        console.error("Failed to update nurse information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  const handlePUTrequest_for_edited_signature = async (nurseId, new_sig) => {
    console.log("new signature being updated");
    try {
      const response = await fetch(
        `https://rpmtms.online/edit-nurse/${nurseId}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: name,
            department: department,
            wards: wards,
            grade: grade,
            signatureImageData: new_sig,
          }),
        }
      );

      if (response.ok) {
        console.log("Nurse information updated successfully");
        // Handle further actions or UI updates
        //console.log("body", signatureImageData);
      } else {
        console.error("Failed to update nurse information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  const handleEditNurse = async (nurseData) => {
    const signatureImageURL = sigCanvasRef.current.toDataURL();
    console.log("signature image uRl:", signatureImageURL);
    const binaryImageData = atob(signatureImageURL.split(",")[1]);
    const imageSizeBytes = binaryImageData.length;

    console.log("signature size", imageSizeBytes);

    if (imageSizeBytes < 3000) {
      //signatureImageURL = signatureImageData
      setSignatureImageData(nurseData.signatureImageData);
      handlePUTrequest(nurseId);

      console.log("less than 3000", nurseData.signatureImageData);
    } else if (imageSizeBytes > 3000) {
      console.log("edited signature of the nurse");
      //setSignatureImageData(toString(signatureImageURL));
      console.log("type", typeof signatureImageURL);
      handlePUTrequest_for_edited_signature(nurseId, signatureImageURL);
    }
    alert("Nurse information edited successfully...")

    navigate("/");
  };

  return (
    <div className="container">
      <h2>Edit Nurse Information</h2>
      <input
        type="text"
        placeholder="Nurse ID"
        value={nurseId}
        onChange={(e) => setNurseId(e.target.value)}
        className="search-input"
      />
      <input
        type="text"
        placeholder="Name"
        value={name}
        onChange={(e) => setName(e.target.value)}
        className="search-input"
      />
      <input
        type="text"
        placeholder="Department"
        value={department}
        onChange={(e) => setDepartment(e.target.value)}
        className="search-input"
      />
      <input
        type="text"
        placeholder="Grade"
        value={grade}
        onChange={(e) => setGrade(e.target.value)}
        className="search-input"
      />
      <textarea
        placeholder="Wards (comma-separated)"
        value={wards.join(", ")}
        onChange={(e) => setWards(e.target.value.split(", "))}
        className="search-input"
      />
      <SignatureCanvas
        ref={sigCanvasRef}
        penColor="black"
        canvasProps={{
          width: 500,
          height: 200,
          className: "image-container",
        }}
      />
      <button onClick={() => handleEditNurse(nurseData)}>Edit Nurse</button>
    </div>
  );
}

export default EditNurseForm;
