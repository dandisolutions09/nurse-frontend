import React, { useRef, useState } from "react";
import {
  Button,
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  Radio,
  RadioGroup,
  Stack,
  Text,
  Select,
  Box,
  HStack,
  CheckboxGroup,
  Checkbox,
} from "@chakra-ui/react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";

import { useToast } from "@chakra-ui/react";
import { color } from "framer-motion";
import "./Search.css"; // Import your CSS file
import ReactSignatureCanvas from "react-signature-canvas";
import axios from "axios";
import { useNavigate } from "react-router-dom";

// import usePostRequest from "../hook/usePostRequest";
// import ErrorAlertComponent from "../components2/ErrorAlert";
// import SuccessAlertComponent from "../components2/SuccessAlert";

const RegistrationFormFinal = () => {
  const [formName, setName] = useState("");
  const [value, setValue] = useState("Admin Form");
  const [showExtraFields, setShowExtraFields] = useState(false);
  const toast = useToast();
  const navigate = useNavigate();

  //   const { loading, error, data, post } = usePostRequest();
  const [accepted, setAccepted] = useState(false);

  const signupInformationRef = useRef({
    name: "",
    // last_name: "",
    grade: "",
    // email: "",
    // disabled: false,
    department: "",
    // role: "Admin",
    // hashed_pwd: "",
    wards: [],
    signatureImageData: "",
  });

  const sigCanvasRef = React.useRef(null);

  const clearSignature = () => {
    sigCanvasRef.current.clear();
  };

  const handleAccept = () => {
    // Perform any actions needed when the user accepts the agreement.
    setAccepted(true);
  };
  const postData = async (data) => {
    console.log("data to be sent", data);
    try {
      const response = await axios.post(
        "https://rpmtms.online/your-fastapi-endpoint2",
        data
      );

      // Handle the response from the backend if needed
      console.log("Response from the backend:", response.data);
    } catch (error) {
      // Handle any errors that occur during the request
      console.error("Error sending data to the backend:", error);
    }
    // alert("nurse saved successfully...");

    navigate("/");
  };

  const handleSubmit = (values, actions) => {
    if (sigCanvasRef.current.isEmpty()) {
      alert("Please provide your signature.");
      return;
    }

    const signatureImageURL = sigCanvasRef.current.toDataURL();

    console.log("reg", values.Name);
    signupInformationRef.current.name = values.Name;
    signupInformationRef.current.grade =
      values.grade !== undefined ? values.grade : "empty";
    signupInformationRef.current.department =
      values.department !== undefined ? values.department : "empty";
    signupInformationRef.current.wards = values.wards; // Add selected wards to signupInformationRef
    signupInformationRef.current.signatureImageData = signatureImageURL;
    console.log("sign up info:-", signupInformationRef.current);
    // // post("http://localhost:8000/register-user", signupInformationRef.current);
    postData(signupInformationRef.current);

    toast({
      title: "registered successfully",
      position: "top",
      isClosable: true,
      duration: 2000,
      status: "success",
    });

    actions.resetForm();
    clearSignature();

    // window.location.reload();
  };

  const validationSchema = Yup.object().shape({
    Name: Yup.string().required(" name is required"),
    department: Yup.string().required("department  is required"),
    grade: Yup.string().required("grade  is required"),
  });

  return (
    <Box ml="400px" className="image-container">
      <Text fontSize="5xl">Register New Nurse</Text>

      <Formik
        initialValues={{
          Name: "",
          department: "",
          grade: "",
        }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        {(formikProps) => (
          <>
            <Form>
              <Box mt={4} w="100%">
                <Field name="Name">
                  {({ field, form }) => (
                    <FormControl
                      isInvalid={form.errors.Name && form.touched.Name}
                    >
                      <FormLabel htmlFor="Name">
                        <Text fontSize="sm" as="b">
                          {" "}
                          Name:
                        </Text>
                      </FormLabel>
                      <Input
                        {...field}
                        id="Name"
                        placeholder="Name"
                        // value={formName}
                      />{" "}
                      <FormErrorMessage>{form.errors.Name}</FormErrorMessage>
                    </FormControl>
                  )}
                </Field>
              </Box>

              <Box mt={6} w="100%">
                <Text fontSize="sm" as="b">
                  {" "}
                  Department:
                </Text>
                <Field name="department">
                  {({ field, form }) => (
                    <FormControl
                      isInvalid={form.errors.Name && form.touched.Name}
                    >
                      <Select
                        {...field}
                        placeholder="--Select Nurse Department--"
                        onChange={(e) =>
                          form.setFieldValue("department", e.target.value)
                        }
                      >
                        {/* Add options for the Select here */}
                        <option value="West">West</option>
                        <option value="North">North </option>
                        <option value="33rd">33rd</option>
                        <option value="4300">4300</option>
                        <option value="3739">3739</option>
                        <option value="Buursma">Buursma</option>
                        <option value="Decade">Decade</option>
                        <option value="B&G">B&G</option>
                      </Select>
                      <FormErrorMessage>
                        {form.errors.department}
                      </FormErrorMessage>
                    </FormControl>
                  )}
                </Field>
              </Box>

              <Box mt={4}>
                <Field name="wards">
                  {({ field, form }) => (
                    <FormControl
                      isInvalid={form.errors.Name && form.touched.Name}
                    >
                      <FormLabel htmlFor="Name">
                        {" "}
                        <Text fontSize="sm" as="b">
                          {" "}
                          Select Wards:
                        </Text>
                      </FormLabel>
                      <CheckboxGroup
                        colorScheme="green"
                        // defaultValue={["naruto", "kakashi"]}
                        value={formikProps.values.wards} // Use formikProps to set and update the value
                        onChange={(selectedWards) =>
                          formikProps.setFieldValue("wards", selectedWards)
                        }
                      >
                        <Box className="image-container">
                          <Stack spacing={[1, 5]} direction={["row"]}>
                            <Checkbox value="A">A</Checkbox>
                            <Checkbox value="B">B</Checkbox>
                            <Checkbox value="C">C</Checkbox>
                          </Stack>
                        </Box>
                      </CheckboxGroup>
                      <FormErrorMessage>{form.errors.wards}</FormErrorMessage>
                    </FormControl>
                  )}
                </Field>
              </Box>


              <Field name="grade">
                  {({ field, form }) => (
                    <FormControl
                      isInvalid={form.errors.Name && form.touched.Name}
                    >
                      <FormLabel htmlFor="Position">
                        <Text fontSize="sm" as="b">
                          {" "}
                          Position:
                        </Text>
                      </FormLabel>
                      <Input
                        {...field}
                        id="Position"
                        placeholder="Position"
                        // value={formName}
                      />{" "}
                      <FormErrorMessage>{form.errors.Name}</FormErrorMessage>
                    </FormControl>
                  )}
                </Field>

              {/* <Box mt={4} w="100%">
                <Text fontSize="sm" as="b">
                  {" "}
                  Position:
                </Text>
                <Field name="grade">
                  {({ field, form }) => (
                    <FormControl
                      isInvalid={form.errors.Name && form.touched.Name}
                    >
                      <Select
                        {...field}
                        placeholder="--Select Nurse Grade--"
                        onChange={(e) =>
                          form.setFieldValue("grade", e.target.value)
                        }
                      >
                        <option value="West">West</option>
                        <option value="North">North </option>
                        <option value="33rd">33rd</option>
                        <option value="4300">4300</option>
                        <option value="3739">3739</option>
                        <option value="Buursma">Buursma</option>
                        <option value="Decade">Decade</option>
                        <option value="B&G">B&G</option>
                      </Select>
                      <FormErrorMessage>
                        {form.errors.department}
                      </FormErrorMessage>
                    </FormControl>
                  )}
                </Field>
              </Box> */}

              <Box mt={0}>
                <Text fontSize="lg">Sign Here:</Text>

                <ReactSignatureCanvas
                  ref={sigCanvasRef}
                  penColor="blue"
                  canvasProps={{
                    width: 400,
                    height: 130,
                    className: "signature-canvas",
                    className: "image-container",
                  }}
                />

                <Box>
                  <Button onClick={clearSignature}>Clear Signature</Button>
                </Box>
              </Box>

              <Button
                size="lg"
                mt={6}
                colorScheme="teal"
                isLoading={formikProps.isSubmitting}
                type="submit"
                disabled={formikProps.isValid} // Disable the button if the form is not valid
                width="500px"
              >
                Submit
              </Button>
            </Form>
          </>
        )}
      </Formik>
    </Box>
  );
};

export default RegistrationFormFinal;
