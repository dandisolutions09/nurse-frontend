import React, { useEffect, useState } from 'react';

const ImageDisplay = ({ base64Data }) => {
  const [imageUrl, setImageUrl] = useState('');

  useEffect(() => {
    // Convert the Base64 data to a binary string
    const binaryString = atob(base64Data);

    // Convert the binary string to an array buffer
    const arrayBuffer = new ArrayBuffer(binaryString.length);
    const uint8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < binaryString.length; i++) {
      uint8Array[i] = binaryString.charCodeAt(i);
    }

    // Create a Blob from the array buffer
    const blob = new Blob([arrayBuffer], { type: 'image/png' }); // Adjust the type according to your image format

    // Create an object URL from the Blob
    const url = URL.createObjectURL(blob);

    setImageUrl(url);

    // Cleanup the object URL when the component unmounts
    return () => URL.revokeObjectURL(url);
  }, [base64Data]);

  return (
    <div>
      {imageUrl ? <img src={imageUrl} alt="Uploaded Image" /> : <p>Loading...</p>}
    </div>
  );
};

export default ImageDisplay;
