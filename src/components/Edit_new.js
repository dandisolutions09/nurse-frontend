import React, { useRef, useState } from "react";
import {
  Button,
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  Radio,
  RadioGroup,
  Stack,
  Text,
  Select,
  Box,
  HStack,
  CheckboxGroup,
  Checkbox,
  Image,
  Heading,
} from "@chakra-ui/react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";

import { useToast } from "@chakra-ui/react";
import { color } from "framer-motion";
import "./Search.css"; // Import your CSS file
import ReactSignatureCanvas from "react-signature-canvas";
import axios from "axios";
import { useLocation, useNavigate } from "react-router-dom";

// import usePostRequest from "../hook/usePostRequest";
// import ErrorAlertComponent from "../components2/ErrorAlert";
// import SuccessAlertComponent from "../components2/SuccessAlert";

const EditFormFinal = () => {
  const location = useLocation(); // Use the useLocation hook to get the location state
  const nurseData = location.state;
  const navigate = useNavigate();

  const toast = useToast();

  //   const { loading, error, data, post } = usePostRequest();
  const [accepted, setAccepted] = useState(false);

  const signupInformationRef = useRef({
    name: "",
    // last_name: "",
    grade: "",
    // email: "",
    // disabled: false,
    department: "",
    // role: "Admin",
    // hashed_pwd: "",
    wards: [],
    signatureImageData: "",
  });

  const sigCanvasRef = React.useRef(null);

  const [nurseId, setNurseId] = useState(nurseData._id); // Nurse ObjectId
  const [name, setName] = useState(nurseData.name);
  const [department, setDepartment] = useState(nurseData.department);
  const [wards, setWards] = useState([nurseData.wards]);
  const [grade, setGrade] = useState(nurseData.grade);
  const [signatureImageData, setSignatureImageData] = useState(
    nurseData.signatureImageData
  );

  const clearSignature = () => {
    sigCanvasRef.current.clear();
  };

  const handleAccept = () => {
    // Perform any actions needed when the user accepts the agreement.
    setAccepted(true);
  };
  const postData = async (data) => {
    console.log("data to be sent", data);
    try {
      const response = await axios.post(
        "https://rpmtms.online/your-fastapi-endpoint2",
        data
      );

      // Handle the response from the backend if needed
      console.log("Response from the backend:", response.data);
    } catch (error) {
      // Handle any errors that occur during the request
      console.error("Error sending data to the backend:", error);
    }
    // alert("nurse saved successfully...");

    navigate("/");
  };

  const handleSubmit = (values, actions) => {
    if (sigCanvasRef.current.isEmpty()) {
      alert("Please provide your signature.");
      return;
    }

    const signatureImageURL = sigCanvasRef.current.toDataURL();

    console.log("reg", values.Name);
    signupInformationRef.current.name = values.Name;
    signupInformationRef.current.grade =
      values.grade !== undefined ? values.grade : "empty";
    signupInformationRef.current.department =
      values.department !== undefined ? values.department : "empty";
    signupInformationRef.current.wards = values.wards; // Add selected wards to signupInformationRef
    signupInformationRef.current.signatureImageData = signatureImageURL;
    console.log("sign up info:-", signupInformationRef.current);
    // // post("http://localhost:8000/register-user", signupInformationRef.current);
    postData(signupInformationRef.current);

    toast({
      title: "registered successfully",
      position: "top",
      isClosable: true,
      duration: 2000,
      status: "success",
    });

    actions.resetForm();
    clearSignature();

    // window.location.reload();
  };

  const validationSchema = Yup.object().shape({
    Name: Yup.string().required(" name is required"),
    department: Yup.string().required("department  is required"),
    grade: Yup.string().required("grade  is required"),
  });

  const handlePUTrequest = async (nurseId, updated_nurse_info) => {
    console.log("updated nurse info:", updated_nurse_info);
    try {
      const response = await fetch(
        `https://rpmtms.online/edit-nurse${nurseId}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: updated_nurse_info.Name,
            department: updated_nurse_info.department,
            wards: updated_nurse_info.wards,
            grade: updated_nurse_info.grade,
            signatureImageData: signatureImageData,
          }),
        }
      );

      if (response.ok) {
        console.log("Nurse information updated successfully");
        // Handle further actions or UI updates
        //console.log("body", signatureImageData);
      } else {
        console.error("Failed to update nurse information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  const handlePUTrequest_for_edited_signature = async (
    nurseId,
    new_sig,
    updated_nurse_info
  ) => {
    console.log("new signature being updated", nurseId);
    try {
      const response = await fetch(
        `https://rpmtms.online/edit-nurse/${nurseId}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: updated_nurse_info.Name,
            department: updated_nurse_info.department,
            wards: updated_nurse_info.wards,
            grade: updated_nurse_info.grade,
            signatureImageData: new_sig,
          }),
        }
      );

      if (response.ok) {
        console.log("Nurse information updated successfully");
        // Handle further actions or UI updates
        //console.log("body", signatureImageData);
      } else {
        console.error("Failed to update nurse information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  const handleEditNurse = async (values, nurseData) => {
    //console.log("edited value passed", values);
    const signatureImageURL = sigCanvasRef.current.toDataURL();
    //console.log("signature image uRl:", signatureImageURL);
    const binaryImageData = atob(signatureImageURL.split(",")[1]);
    const imageSizeBytes = binaryImageData.length;

    //console.log("signature size", imageSizeBytes);

    if (imageSizeBytes < 3000) {
      //signatureImageURL = signatureImageData
      setSignatureImageData(nurseData.signatureImageData);
      handlePUTrequest(nurseId, values);

      //console.log("less than 3000", nurseData.signatureImageData);
    } else if (imageSizeBytes > 3000) {
      //console.log("edited signature of the nurse");
      //setSignatureImageData(toString(signatureImageURL));
      console.log("type", typeof signatureImageURL);
      handlePUTrequest_for_edited_signature(nurseId, signatureImageURL, values);
    }
    toast({
      title: "Nurse information edited successfully...",
      position: "top",
      isClosable: true,
      duration: 2000,
      status: "success",
    });

    navigate("/");
  };

  return (
    <Box ml="400px" className="image-container">
      <Text fontSize="5xl">Edit Nurse Info</Text>

      <Formik
        initialValues={{
          Name: nurseData.name,
          department: nurseData.department,
          grade: nurseData.grade,
          wards: nurseData.wards,
        }}
        // onSubmit={handleSubmit}
        onSubmit={handleEditNurse}
        validationSchema={validationSchema}
      >
        {(formikProps) => (
          <>
            <Form>
              <Box mt={4} w="100%">
                <Field name="Name">
                  {({ field, form }) => (
                    <FormControl
                      isInvalid={form.errors.Name && form.touched.Name}
                    >
                      <FormLabel htmlFor="Name">
                        <Text fontSize="sm" as="b">
                          {" "}
                          Name:
                        </Text>
                      </FormLabel>
                      <Input
                        {...field}
                        id="Name"
                        placeholder="Name"
                        // value={formName}
                        // value={name}
                      />{" "}
                      <FormErrorMessage>{form.errors.Name}</FormErrorMessage>
                    </FormControl>
                  )}
                </Field>
              </Box>

              <Box mt={6} w="100%">
                <Text fontSize="sm" as="b">
                  {" "}
                  Department:
                </Text>
                <Field name="department">
                  {({ field, form }) => (
                    <FormControl
                      isInvalid={form.errors.Name && form.touched.Name}
                    >
                      <Select
                        {...field}
                        placeholder="--Select Nurse Department--"
                        onChange={(e) =>
                          form.setFieldValue("department", e.target.value)
                        }
                      >
                        {/* Add options for the Select here */}
                        <option value="West">West</option>
                        <option value="North">North </option>
                        <option value="33rd">33rd</option>
                        <option value="4300">4300</option>
                        <option value="3739">3739</option>
                        <option value="Buursma">Buursma</option>
                        <option value="Decade">Decade</option>
                        <option value="B&G">B&G</option>
                      </Select>
                      <FormErrorMessage>
                        {form.errors.department}
                      </FormErrorMessage>
                    </FormControl>
                  )}
                </Field>
              </Box>

              <Box mt={4}>
                <Field name="wards">
                  {({ field, form }) => (
                    <FormControl
                      isInvalid={form.errors.Name && form.touched.Name}
                    >
                      <FormLabel htmlFor="Name">
                        {" "}
                        <Text fontSize="sm" as="b">
                          {" "}
                          {/* Wards: {wards} */}
                        </Text>
                      </FormLabel>
                      <CheckboxGroup
                        colorScheme="green"
                        // defaultValue={["naruto", "kakashi"]}
                        value={formikProps.values.wards} // Use formikProps to set and update the value
                        onChange={(selectedWards) =>
                          formikProps.setFieldValue("wards", selectedWards)
                        }
                      >
                        <Box className="image-container">
                          <Stack spacing={[1, 5]} direction={["row"]}>
                            <Checkbox value="A">A</Checkbox>
                            <Checkbox value="B">B</Checkbox>
                            <Checkbox value="C">C</Checkbox>
                          </Stack>
                        </Box>
                      </CheckboxGroup>
                      <FormErrorMessage>{form.errors.wards}</FormErrorMessage>
                    </FormControl>
                  )}
                </Field>
              </Box>
              <Box mb={2}>
                <Field name="grade">
                  {({ field, form }) => (
                    <FormControl
                      isInvalid={form.errors.Name && form.touched.Name}
                    >
                      <FormLabel htmlFor="Position">
                        <Text fontSize="sm" as="b">
                          {" "}
                          Position:
                        </Text>
                      </FormLabel>
                      <Input
                        {...field}
                        id="Position"
                        placeholder="Position"
                        // value={formName}
                      />{" "}
                      <FormErrorMessage>{form.errors.Name}</FormErrorMessage>
                    </FormControl>
                  )}
                </Field>
              </Box>

              {/* <Box mt={4} w="100%">
                <Text fontSize="sm" as="b">
                  {" "}
                  Grade:
                </Text>
                <Field name="grade">
                  {({ field, form }) => (
                    <FormControl
                      isInvalid={form.errors.Name && form.touched.Name}
                    >
                      <Select
                        {...field}
                        placeholder="--Select Nurse Grade--"
                        onChange={(e) =>
                          form.setFieldValue("grade", e.target.value)
                        }
                      >
                        <option value="West">West</option>
                        <option value="North">North </option>
                        <option value="33rd">33rd</option>
                        <option value="4300">4300</option>
                        <option value="3739">3739</option>
                        <option value="Buursma">Buursma</option>
                        <option value="Decade">Decade</option>
                        <option value="B&G">B&G</option>
                      </Select>
                      <FormErrorMessage>
                        {form.errors.department}
                      </FormErrorMessage>
                    </FormControl>
                  )}
                </Field>
              </Box> */}

              <Box className="image-container">
                <Heading mt={2} size="xs" textTransform="uppercase">
                  Original Signature:
                </Heading>

                <Image src={nurseData.signatureImageData} alt="Dan Abramov" />
              </Box>

              <Box mt={0}>
                <Heading mt={2} size="xs" textTransform="uppercase">
                  New Signature:
                </Heading>
                <ReactSignatureCanvas
                  ref={sigCanvasRef}
                  penColor="blue"
                  canvasProps={{
                    width: 400,
                    height: 130,
                    className: "signature-canvas",
                    className: "image-container",
                  }}
                />

                <Box>
                  <Button onClick={clearSignature}>Clear Signature</Button>
                </Box>
              </Box>

              <Button
                size="lg"
                mt={6}
                colorScheme="teal"
                isLoading={formikProps.isSubmitting}
                type="submit"
                disabled={formikProps.isValid} // Disable the button if the form is not valid
                width="500px"
              >
                Submit
              </Button>
            </Form>
          </>
        )}
      </Formik>
    </Box>
  );
};

export default EditFormFinal;
