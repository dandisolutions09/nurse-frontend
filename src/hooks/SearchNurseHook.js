import { useState, useEffect } from 'react';
import axios from 'axios';

function useGetNurseByName() {
  const [nurseData, setNurseData] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);


  const getNurseByName = async (name) => {
    setLoading(true);
    setError(null);

    try {
      //const response = await axios.get(`http://localhost:8000/get-nurse/${name}`); // Adjust the URL as needed
      const response = await axios.get(`https://rpmtms.online/get-nurse/${name}`); // Adjust the URL as needed

      setNurseData(response.data);
      // console.log(response.data)
      if(response.data.message === "Document not found"){
        console.log("docuemnt is empty")
        setError("Nurse name not found!!!");

      }
    } catch (err) {
      console.log(err)
      setError("An error occurred while fetching nurse data.");
    } finally {
      setLoading(false);
    }
  };

  return { nurseData, loading, error, getNurseByName };
}

export default useGetNurseByName;
